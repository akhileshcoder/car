import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'auth',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class Auth {
  http:Http;
  router:Router;
  isLogin = localStorage.getItem('myJWT');
  cred = {
    uname:"",
    pass:""
  };
  constructor(http:Http,router:Router){
    if(!this.isLogin) {
      this.http = http;
      this.router = router;
    }else {
      router.navigateByUrl('dashboard')
    }
  }
  login(){
    this.http.post('/login',
      this.cred).subscribe(data => {
      localStorage.setItem('myJWT',data.json().token);
      this.router.navigateByUrl('dashboard')
    });
  }
}
