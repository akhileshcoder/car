import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Auth }   from '../auth/app.component';
import { Dashboard }      from '../dashboard/app.component';
import { Cashier }      from '../cashier/app.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'auth',  component: Auth },
  { path: 'dashboard',     component: Dashboard },
  { path: 'cashier',     component: Cashier }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
