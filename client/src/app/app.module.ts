import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule }     from './app-routing';
import { AppComponent } from './app.component';
import { Auth }   from '../auth/app.component';
import { Cashier }   from '../cashier/app.component';
import { Dashboard }   from '../dashboard/app.component';
import { BonusCard }   from '../directives/bonus_card/app.component';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent, Auth, Dashboard, Cashier, BonusCard
  ],
  imports: [
    BrowserModule,FormsModule,AppRoutingModule,HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
