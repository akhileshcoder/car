const bodyParser = require('body-parser'),
	cors = require('cors'),
	conf = require('../conf/config'),
	ecode = require('../conf/error_codes');
const jwt  = require('jsonwebtoken');
module.exports.middleware = function(app,express) {
	app.use(cors());
	app.use(bodyParser.json({limit: '5mb'}));
	app.use(bodyParser.urlencoded({extended: true,limit: '5mb'}));
	app.use(express.static(__dirname + '/../../client/dist'));
	app.use('/', function (req, res, next) {
		console.log("req in route");
	  next();
	});
	app.use('/user/', function (req, res, next) {
		console.log("User tokens can be validate here");
        jwt.verify(req.headers.jwt, conf.development.secret, function(err, decoded) {
            if(err){
            	console.log("err: :::: ",err);
                res.send({
					msg:ecode[103],
					errCode:103
				})
			}else {
            	console.log("decoded: ",decoded);
                next();
			}
        });
	});
};