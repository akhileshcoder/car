var errorCodes = {
	100 : "Success message",	
	101 : "Invalid username or password", //invalid username
	102 : "Invalid Authentication", //invalid password
	103 : "invalid token",  //invalid website token when generate game token
	104 : "Domain is not registered",
	105 : "Device is not supported",
	107 : "Invalid  token",
	108	: "token session time expired",
	109 : "Client inactive expire time reached",
	110	: "DB connection error",
	111	: "Begin Transaction error",
	112	: "Querying error to DB",
	113	: "Unable to commit the DB transaction",
	114	: "User Name already Exists",
	115	: "field value is not correct",
	116 : "Can not find user_id",
	117 : "Requested parms can not find",
	118 : "Insufficient Balance to sit",
	119 : "invalid params supplied",
	120 : "already logged out",
	121 : "Already got the promo chips today",
	500 : "Some Server Internal error",
	501 : "Some Server Internal error" //mysql error
};

module.exports = errorCodes;