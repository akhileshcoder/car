var path=require("path"),
    stationApi=require("../controllers/stationApi");
module.exports ={
    "get":[
        {
            "url":["/","/auth","/dashboard"],
            "callback":function(req, res, next){
                res.sendFile('index.html', { root: path.join(__dirname,'../../client/dist') });
            }
        },
        {
            "url":"/user/get_station",
            "callback":function(req, res, next){
                stationApi.getStation('all',res);
            }
        }
    ],
    "post":[
        {
            "url":"/login",
            "callback":function(req, res, next){
                stationApi.login(req.body,res);
            }
        },
        {
            "url":"/user/save_station",
            "callback":function(req, res, next){
                stationApi.saveStation(req.body.station,res);
            }
        },
        {
            "url":"/user/delete_station",
            "callback":function(req, res, next){
                stationApi.deleteStation(req.body._id,res);
            }
        },
        {
            "url":"/user/rate_station",
            "callback":function(req, res, next){
                stationApi.rateStation(req.body,res);
            }
        },
        {
            "url":"/user/make_pament",
            "callback":function(req, res, next){
                stationApi.makePament(req.body,res);
            }
        }
        ],
    "put":[]
};


